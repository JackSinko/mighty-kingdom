﻿using Pixelplacement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    private PlayerManager player;
    private CameraManager cameraManager;
    private void Start() {
        player = GameObject.Find("Player").GetComponent<PlayerManager>();
        cameraManager = Camera.main.GetComponent<CameraManager>();
        UpdateScore();
    }

    public Text scoreText;

    public GameObject multiplierParent; //reference to the parent holding the score UI
    public Vector3 multiplier_poppedUpSize = new Vector3(1.5f, 1.5f, 1.5f);
    public Vector3 multiplier_poppedDownSize = new Vector3(1, 1, 1);

    private int currentMultiplier = 1;
    public int score = 0;
    public void UpdateScore() {
        if(!player.isDead) { 
            //update score
            score += Mathf.CeilToInt((Time.deltaTime / cameraManager.speedSlowDown) * 100);

            //update score UI
            scoreText.text = "" + score;

            //update multiplier UI
            float mutliplierPercentage = (cameraManager.speed-cameraManager.initialSpeed) / cameraManager.speedDifference;
            int newMultiplier = 1;
            if(mutliplierPercentage >= 1f) newMultiplier = 4; //at max speed
            else if(mutliplierPercentage >= 0.66f) newMultiplier = 3; //at >2/3 speed
            else if(mutliplierPercentage >= 0.33f) newMultiplier = 2; //at >1/3 speed

            if(newMultiplier != currentMultiplier) {
                //just changed multiplier
                currentMultiplier = newMultiplier;

                //set new multiplier UI
                for(int count = 1; count <= 4; count++)
                    multiplierParent.transform.Find("x" + count).gameObject.SetActive(count == newMultiplier); //activate if is new multiplier

                //lerp the multiplier UI to show change
                Tween.LocalScale(multiplierParent.transform, multiplier_poppedUpSize, 0.2f, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
                Tween.LocalScale(multiplierParent.transform, multiplier_poppedDownSize, 0.1f, 0.3f, Tween.EaseBounce, Tween.LoopType.None, null, null, false);

                Tween.LocalScale(multiplierParent.transform, multiplier_poppedUpSize, 0.2f, 1.1f, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
                Tween.LocalScale(multiplierParent.transform, multiplier_poppedDownSize, 0.1f, 1.4f, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
            }

        }
    }

}
