﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {

    public Vector3 initialPosition; //initialised on start
    public Quaternion initialRotation; //initialised on start
    public float initialSpeed; //initialised on start
    private PlayerManager player;
    private ScoreManager scoreManager;
    private MenuSystem menuSystem;
    private void Start() {
        player = GameObject.Find("Player").GetComponent<PlayerManager>();    
        scoreManager = GameObject.Find("MapManager").GetComponent<ScoreManager>();
        menuSystem = GameObject.Find("Menus").GetComponent<MenuSystem>();
        initialSpeed = speed;
        speedDifference = maxSpeed - initialSpeed;
        initialPosition = transform.position;
        initialRotation = transform.rotation;
    }

    public float cameraHeight = 15;
    public float speed = 0.15f;
    public float maxSpeed = 0.5f;
    public float speedDifference; //calculated on start
    [Range(1,1000)] public float speedSlowDown = 300;

    public float minCameraHeight = 7;

    private void Update() { 

        if(menuSystem.currentMenu == MenuType.None || menuSystem.currentMenu == MenuType.Death) {

            //don't translate camera's X while player has died
            if(!player.isDead) {

                //increase the speed (difficulty)
                if(speed < maxSpeed)
                    speed += Time.deltaTime / speedSlowDown;
                if(speed > maxSpeed) speed = maxSpeed;

                scoreManager.UpdateScore();

                //lerp camera to new height (players height) - doesn't lerp x translation
                transform.position = Vector3.Lerp(new Vector3(transform.position.x + speed, transform.position.y, transform.position.z), new Vector3(transform.position.x + speed, player.transform.position.y + cameraHeight, transform.position.z), Time.deltaTime);
            } else {
                //has died - do death camera animation:

                if(transform.position.y > minCameraHeight) { //only lerp until player gets to minimum point

                    //lerp to minimum point if player is already below minimum point
                    float height = minCameraHeight;
                    if(player.transform.position.y > minCameraHeight) height = player.transform.position.y;

                    transform.RotateAround(player.deathPosition, Vector3.up, -0.1f); //rotate camera
                    transform.position = Vector3.Lerp(new Vector3(transform.position.x, transform.position.y, transform.position.z), new Vector3(transform.position.x, height, transform.position.z), Time.deltaTime/2);
                }
            }

        }

    }

}
