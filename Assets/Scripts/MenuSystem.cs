﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum MenuType {
    None,
    Main,
    Death,
    Highscore,
    Help
}

public class MenuSystem : MonoBehaviour { 

    private GameObject mainMenu;
    private GameObject deathMenu;
    private GameObject highscoreMenu;
    private GameObject helpMenu;

    public MenuType currentMenu; //if player is currently in menu

    public void LoadGame() {
        Show((int)MenuType.None); //hide menus
        
        //reset time scale
        Time.timeScale = 1;

        //reset stats
        player.isDead = false;
        scoreManager.score = 0;
        cameraManager.speed = cameraManager.initialSpeed;
        player.boostMeter_Used = player.boostMeter_Max; //player starts off with no boost
        player.numberOfBoosts = 0;

        //update the score
        scoreManager.UpdateScore();

        //reset item spawning
        itemManager.PickNewPlatform(ItemType.BOMB);
        itemManager.PickNewPlatform(ItemType.BOOST);

        //remove old map
        Item[] items = FindObjectsOfType<Item>();
        foreach(Item item in items) {
            if(item.name != "StartingOcean" && item.name != "StartingPlatform")
                Destroy(item.gameObject);
        }

        //remove any old particles
        ParticleSystem[] particles = FindObjectsOfType<ParticleSystem>();
        foreach(ParticleSystem particle in particles) {
            if(!particle.CompareTag("DontDestroy"))
                Destroy(particle.gameObject);
        }

        //reset map generation
        mapManager.numberOfOceans = 1;
        mapManager.numberOfPlatforms = 1;
        if(GameObject.Find("StartingPlatform") == null)
            mapManager.lastPlatform = Instantiate(mapManager.platformPrefab, mapManager.initialPlatformPosition, Quaternion.identity);
        else mapManager.lastPlatform = GameObject.Find("StartingPlatform");
        if(GameObject.Find("StartingOcean") == null)
            mapManager.lastOcean = Instantiate(mapManager.oceanPrefab, mapManager.initialOceanPosition, Quaternion.identity);
        else mapManager.lastOcean = GameObject.Find("StartingOcean");

        //reset positions
        player.gameObject.transform.position = mapManager.startingPos;
        cameraManager.gameObject.transform.position = cameraManager.initialPosition;
        cameraManager.gameObject.transform.rotation = cameraManager.initialRotation;
    }

    public List<int> scores = new List<int>(); //a list of scores in order of runs

    private CameraManager cameraManager;
    private ScoreManager scoreManager;
    private MapManager mapManager;
    private PlayerManager player;
    private ItemSpawner itemManager;
    private void Start() {
        scoreManager = GameObject.Find("MapManager").GetComponent<ScoreManager>();
        cameraManager = Camera.main.GetComponent<CameraManager>();
        mapManager = GameObject.Find("MapManager").GetComponent<MapManager>();
        player = GameObject.Find("Player").GetComponent<PlayerManager>();
        itemManager = GameObject.Find("MapManager").GetComponent<ItemSpawner>();

        //menus are children of the MenuSystem:
        mainMenu = transform.Find("Main").gameObject; 
        deathMenu = transform.Find("Death").gameObject;
        highscoreMenu = transform.Find("Highscore").gameObject;
        helpMenu = transform.Find("Help").gameObject;

        //enable main menu on start
        Show((int)MenuType.Main);
    }

    public List<Text> leaderboard; //list of texts in order (leaderboard)

    public void Show(int menuID) {
        //only set the new menu as active:
        MenuType menu = (MenuType) menuID;
        mainMenu.SetActive(menu == MenuType.Main);
        deathMenu.SetActive(menu == MenuType.Death);
        highscoreMenu.SetActive(menu == MenuType.Highscore);
        helpMenu.SetActive(menu == MenuType.Help);

        currentMenu = menu; //update current menu

        //highscore menu:
        if(menu == MenuType.Highscore) {
            //sort scores in descending order
            scores.Sort((a, b) => b.CompareTo(a));

            //display top highscores
            for(int count = 0; count < leaderboard.Count; count++) {
                if(count <= (scores.Count-1) && leaderboard[count] != null) //score exists and leaderboard space exists
                    leaderboard[count].text = "" + scores[count];
            }
        }
    }

}
