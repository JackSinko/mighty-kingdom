﻿using System.Collections;
using System.Collections.Generic;
using Pixelplacement;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerManager : MonoBehaviour { 
    
    public bool isDead = false;
    public Vector3 deathPosition;

    private bool jetPackParticlesPlaying = true;
    public float extraMutliplier_Running = 2f;
    public float extraMutliplier_Flying = 8.5f;
    private float startingJetPackHorizontalForce;

    public float jetPackMeter_Max = 1.1f; //in seconds
    private float jetPackMeter_Used = 0; //in seconds

    private bool isFlying = false;
    public float jetpackHeightForce = 17.5f;
    public float jetpackHorizontalForce = 6;

    public float boostAmount = 1.7f;
    private bool isBoosting = false;
    public float boostMeter_Max = 0.5f; //in seconds
    public float boostMeter_Used; //initialised on start
    public int numberOfBoosts = 0;
    private bool boostHelperHasBeenUsed = false;

    public SimpleHealthBar rocketFuelMetre;
    public SimpleHealthBar boostPowerMetre;

    private CameraManager cameraManager;
    private ThirdPersonCharacter controller;
    private void Start() {
        controller = GetComponent<ThirdPersonCharacter>();
        startingJetPackHorizontalForce = jetpackHorizontalForce; //get initial jetpack force
        boostMeter_Used = boostMeter_Max; //player starts off with no boost
        cameraManager = Camera.main.GetComponent<CameraManager>();
    }

    public ParticleSystem rightJetPack;
    public ParticleSystem leftJetPack;

    public bool MeterIsFull() {
        if(jetPackMeter_Used > jetPackMeter_Max) jetPackMeter_Used = jetPackMeter_Max; //ensure it doesn't go over max
        return jetPackMeter_Used == jetPackMeter_Max;
    }

    public bool BoostIsEmpty() {
        if(boostMeter_Used > boostMeter_Max) boostMeter_Used = boostMeter_Max; //ensure it doesn't go over max
        return boostMeter_Used == boostMeter_Max;
    }

    private void Update() {

        //speed of camera gets faster - player speed changes in relation to camera speed
        float flyingSpeedMultiplier = 1 + (cameraManager.speed * extraMutliplier_Flying);

        //make the player faster as the camera gets faster
        controller.m_MoveSpeedMultiplier = 0.467f + (cameraManager.speed * extraMutliplier_Running); //running speed
        controller.m_AnimSpeedMultiplier = 0.467f + (cameraManager.speed * extraMutliplier_Running); //running anim speed
        jetpackHorizontalForce = startingJetPackHorizontalForce * flyingSpeedMultiplier; //flying speed
    
        //calculate force:
        float force = CrossPlatformInputManager.GetAxis("Horizontal") < 0 ? -jetpackHorizontalForce : jetpackHorizontalForce;
        if(CrossPlatformInputManager.GetAxis("Horizontal") == 0) force = 0;

        if(CrossPlatformInputManager.GetButton("Boost") && !isDead) {
            //attempting to use boost

            //need to recharge?
            if(BoostIsEmpty() && numberOfBoosts > 0) {
                numberOfBoosts--; //use a boost
                boostMeter_Used = 0; //replenlish boost
                UpdateBoost(); //lerp scale boost (to represent it was used):
            }

            //reset jetpack meter as helps with first boost
            if(!boostHelperHasBeenUsed && isBoosting == false && !BoostIsEmpty()) {
                jetPackMeter_Used = 0; 
                boostHelperHasBeenUsed = true;
            }

            if(!BoostIsEmpty()) { 
                boostMeter_Used += Time.deltaTime;
                isBoosting = true;
                controller.m_Rigidbody.velocity = new Vector3(force*boostAmount,jetpackHeightForce*(boostAmount/1.25f),0);
            } else if(numberOfBoosts == 0) {
                //boost is maxed and none left
                isBoosting = false; //just stopped boosting
                UpdateBoost(); //lerp scale boost (to represent 0 left)
            }
        } else isBoosting = false; //just stopped boosting

        if(CrossPlatformInputManager.GetButton("Fly") && (!MeterIsFull() && !isBoosting) && !isStunned && !isDead) { 
            //using jetpack
            isFlying = true;
		    controller.m_Rigidbody.velocity = new Vector3(force,jetpackHeightForce,0);
        } else isFlying = false; //just stopped flying

        if(isFlying || isBoosting) { 
            if(!jetPackParticlesPlaying) { //just started flying
                //play jetpack particles
                rightJetPack.Play();
                leftJetPack.Play();
                jetPackParticlesPlaying = true;
            }
            //use jetpack fuel
            if(isFlying && !isBoosting && !MeterIsFull()) //only use fuel if flying (not boosting)
                jetPackMeter_Used += Time.deltaTime;
        }
        if(!isFlying && !isBoosting) {
            if(jetPackParticlesPlaying) { //just stopped flying
                //stop jetpack particles
                rightJetPack.Stop();
                leftJetPack.Stop();
                jetPackParticlesPlaying = false;
            }
            if(controller.m_IsGrounded) {
                jetPackMeter_Used = 0; //reset jetpack meter if grounded
                boostHelperHasBeenUsed = false; //reset helper
            }
        }
        rocketFuelMetre.UpdateBar(jetPackMeter_Max-jetPackMeter_Used, jetPackMeter_Max); //update the rocket power metre bar
        boostPowerMetre.UpdateBar(boostMeter_Max-boostMeter_Used, boostMeter_Max); //update the rocket power metre bar
    }

    //updating boost UI:
    private bool boosts_isUpdating;
    public GameObject boostsObject;
    public GameObject boostsObject_any;
    public GameObject boostsObject_0;
    public Text numberOfBoostsText;
    public Vector3 boosts_poppedUpSize = new Vector3(0.3f, 0.3f, 0.3f);
    public Vector3 boosts_poppedDownSize = new Vector3(0.15f, 0.15f, 0.15f);
    public void UpdateBoost() {

        int numberOfBoostsIncludingCurrent = numberOfBoosts;
        if(!BoostIsEmpty()) numberOfBoostsIncludingCurrent++;

        //update the image for boosts
        bool hasBoosts = numberOfBoostsIncludingCurrent > 0;
        boostsObject_0.SetActive(!hasBoosts);
        boostsObject_any.SetActive(hasBoosts);

        //update the text with number of boosts
        numberOfBoostsText.text = "" + numberOfBoostsIncludingCurrent;

        if(!boosts_isUpdating) {
            //lerp the boosts image if not already updating
            boosts_isUpdating = true;
            Tween.LocalScale(boostsObject.transform, boosts_poppedUpSize, 0.2f, 0f, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
            Tween.LocalScale(boostsObject.transform, boosts_poppedDownSize, 0.1f, 0.3f, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
            StartCoroutine(WaitForBoostLerp());
        }

        boostPowerMetre.UpdateBar(boostMeter_Max-boostMeter_Used, boostMeter_Max);
    }
    IEnumerator WaitForBoostLerp() { 
        yield return new WaitForSeconds(0.4f); //takes 0.4 seconds
        boosts_isUpdating = false;
    }

    //kill the player to play death particles and set as dead
    public GameObject jetpack; //reference to jetpack on player
    public GameObject deathParticles;
    public void Kill() {
        isDead = true;
        deathPosition = transform.position;

        //instantiate death particles
        GameObject particles = Instantiate(deathParticles, deathPosition, Quaternion.identity);
        Destroy(particles, 10f);
    }

    public float stunTime = 0.5f;
    private bool isStunned = false;
    public void Stun(Vector3 explosionPos) {
        isStunned = true;
        controller.m_Rigidbody.AddExplosionForce(100, explosionPos, 2, 10, ForceMode.VelocityChange);
        StartCoroutine(WaitForStun());
    }
    IEnumerator WaitForStun() { 
        yield return new WaitForSeconds(stunTime);
        isStunned = false;
    }
}
