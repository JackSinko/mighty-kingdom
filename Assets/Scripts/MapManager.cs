﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//responsible for generating the map
public class MapManager : MonoBehaviour { 

    //OCEAN:
    public int maxNumberOfOceans = 2;
    public int numberOfOceans = 1; //the number of oceans in the scene
    public GameObject oceanPrefab;
    public Vector3 initialOceanPosition;
    public GameObject lastOcean; //reference to the last ocean spawned
    public float spacingBetweenOceans = 750; //where to spawn new oceans

    //PLATFORMS:
    public int maxNumberOfPlatforms = 15; //the number of platforms allowed in the scene
    public int numberOfPlatforms = 1; //the number of platforms in the scene
    public GameObject platformPrefab;
    public Vector3 initialPlatformPosition;
    public GameObject lastPlatform; //reference to the last generated platform
    public int totalPlatformsGenerated = 0;

    public float platformDistance_Min = 1; //in metres
    public float platformDistance_Max = 15; //in metres

    public float platformHeight_Min = 7;
    public float platformHeight_Max = 15;

    public float platformWidth_Min = 5;
    public float platformWidth_Max = 15;

    private PlayerManager player;
    public Vector3 startingPos; //the position that the player starts in
    private ItemSpawner itemSpawner;
    private MenuSystem menuSystem;
    private void Start() {
        itemSpawner = GameObject.Find("MapManager").GetComponent<ItemSpawner>();
        player = GameObject.Find("Player").GetComponent<PlayerManager>();
        menuSystem = GameObject.Find("Menus").GetComponent<MenuSystem>();
        startingPos = player.transform.position; //set initial starting position as the players position
        lastPlatform = GameObject.Find("StartingPlatform");
        lastOcean = GameObject.Find("StartingOcean");
        initialOceanPosition = lastOcean.transform.position;
        initialPlatformPosition = lastPlatform.transform.position;
    }

    public void Update() {
        if(menuSystem.currentMenu == MenuType.None) {
            if(numberOfPlatforms < maxNumberOfPlatforms) {
                SpawnPlatform(); //ensure there is always maxNumberOfPlatforms number of platforms
            }
            if(numberOfOceans < maxNumberOfOceans) {
                //spawn an ocean
                numberOfOceans++;
                Vector3 lastOceanPos = lastOcean.transform.position;
                Vector3 position = new Vector3(lastOceanPos.x + spacingBetweenOceans, lastOceanPos.y, lastOceanPos.z);
                lastOcean = Instantiate(oceanPrefab, position, Quaternion.identity);
            }
        }
    }
    
    public void SpawnPlatform() {

        numberOfPlatforms++;

        float randomHeight = Random.Range(platformHeight_Min, platformHeight_Max);
        float randomWidth = Random.Range(platformWidth_Min, platformWidth_Max);

        float xCoordOfLastPlatform = lastPlatform.transform.position.x + (lastPlatform.transform.localScale.x / 2); //the x coord of edge of platform
        float randomDistance = Random.Range(platformDistance_Min, platformDistance_Max);
        float widthOfPlatform = randomWidth / 2; //the first edge of new platform
        Vector3 position = new Vector3(xCoordOfLastPlatform + randomDistance + widthOfPlatform, 0, 0);

        lastPlatform = Instantiate(platformPrefab, position, Quaternion.identity);
        lastPlatform.transform.localScale = new Vector3(randomWidth, randomHeight, lastPlatform.transform.localScale.z);
        lastPlatform.transform.position = new Vector3(lastPlatform.transform.position.x, (randomHeight/2), lastPlatform.transform.position.y);

        totalPlatformsGenerated++;

        //get position on platform that item can spawn:
        float xPos_Min = position.x - widthOfPlatform;
        float xPos_Max = position.x + widthOfPlatform;
        float yPos = randomHeight + itemSpawner.howHighItemSits;

        if(itemSpawner.CanSpawn(ItemType.BOOST)) {

            //get platforms item spawn position
            Vector3 positionToSpawn = new Vector3(Random.Range(xPos_Min, xPos_Max), yPos, 0);

            //spawn boost on platform
            Instantiate(itemSpawner.boostPrefab, positionToSpawn, Quaternion.Euler(new Vector3(90,0,0)));
            itemSpawner.PickNewPlatform(ItemType.BOOST);

        }

        if(itemSpawner.CanSpawn(ItemType.BOMB)) {

            //get platforms item spawn position
            Vector3 positionToSpawn = new Vector3(Random.Range(xPos_Min, xPos_Max), yPos, 0);

            //spawn boost on platform
            Instantiate(itemSpawner.bombPrefab, positionToSpawn, Quaternion.Euler(new Vector3(90,0,0)));
            itemSpawner.PickNewPlatform(ItemType.BOMB);

        }

    }

}
