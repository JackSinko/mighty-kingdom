﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//derives from Item
public class PickupableItem : Item {
    
    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player")) {
            OnPickup();
        }
    }

    virtual protected void OnPickup() { }
}
