﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemType {
    OCEAN,
    PLATFORM,
    BOOST,
    BOMB
};

//put script on any object that must despawn when out of camera
public class Item : MonoBehaviour {

    public ItemType type;

    protected PlayerManager player;
    protected MapManager mapManager;
    protected ItemSpawner itemManager;
    protected MenuSystem menuSystem;
    protected ScoreManager scoreManager;
    private void Start() {
        mapManager = GameObject.Find("MapManager").GetComponent<MapManager>();
        player = GameObject.Find("Player").GetComponent<PlayerManager>();
        itemManager = GameObject.Find("MapManager").GetComponent<ItemSpawner>();
        menuSystem = GameObject.Find("Menus").GetComponent<MenuSystem>();
        scoreManager = GameObject.Find("MapManager").GetComponent<ScoreManager>();
    }

    //destroy any items that are off the screen
    void OnBecameInvisible() {
        if(transform.position.x < Camera.main.transform.position.x) {
            if(type == ItemType.PLATFORM) mapManager.numberOfPlatforms--;
            if(type == ItemType.OCEAN) mapManager.numberOfOceans--;
            Destroy(gameObject);
        }
    }
}
