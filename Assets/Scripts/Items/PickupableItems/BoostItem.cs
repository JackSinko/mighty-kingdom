﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostItem : PickupableItem {

    override protected void OnPickup() {
        if(player.BoostIsEmpty()) player.boostMeter_Used = 0; //refill boost
        else player.numberOfBoosts++; //add 1 to boost
        player.UpdateBoost();
        Destroy(gameObject);
    }

}
