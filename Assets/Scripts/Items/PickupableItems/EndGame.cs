﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//put this on any object that if the player collides with, will end the game
public class EndGame : PickupableItem {

    override protected void OnPickup() {
        //when player hits ocean collider, end the game
        RunEndOfGame();
    }

    public void RunEndOfGame() {
        PlayerManager player = GameObject.Find("Player").GetComponent<PlayerManager>();
        if(!player.isDead) { //don't kill twice
            player.Kill();

            Time.timeScale = 0.25f; //slow down time

            //show death screen
            menuSystem.Show((int)MenuType.Death);

            //save highscore
            menuSystem.scores.Add(scoreManager.score);

            //reset item spawning
            itemManager.currentPlatformToSpawn_Boost = 0;
            itemManager.currentPlatformToSpawn_Bomb = 0;
        }
    }

}
