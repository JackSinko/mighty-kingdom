﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombItem : PickupableItem {

    override protected void OnPickup() {
        //stun player
        player.Stun(transform.position);
        GameObject bombParticles = Instantiate(itemManager.bombParticles, transform.position, Quaternion.identity);
        Destroy(bombParticles, 3f);
        Destroy(gameObject);
    }

}
