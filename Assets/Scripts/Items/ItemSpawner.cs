﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour { 

    public GameObject boostPrefab;
    public GameObject bombPrefab;

    public GameObject bombParticles;

    private MapManager mapManager;
    private void Start() {
        mapManager = GameObject.Find("MapManager").GetComponent<MapManager>();
        PickNewPlatform(ItemType.BOMB);
        PickNewPlatform(ItemType.BOOST);
    }

    public float howHighItemSits = 1;

    public int spawn_Min_Boost = 10; //the minimum number of platforms between boosts spawning
    public int spawn_Max_Boost = 20; //the maximum number of platforms between boosts spawning
    public int currentPlatformToSpawn_Boost;

    public int spawn_Min_Bomb = 2; //the minimum number of platforms between bombs spawning
    public int spawn_Max_Bomb = 15; //the maximum number of platforms between bombs spawning
    public int currentPlatformToSpawn_Bomb;

    public bool CanSpawn(ItemType type) {
        if(type == ItemType.BOOST) { 
            if(mapManager.totalPlatformsGenerated == currentPlatformToSpawn_Boost) {
                return true;
            }
        }
        if(type == ItemType.BOMB) { 
            if(mapManager.totalPlatformsGenerated == currentPlatformToSpawn_Bomb) {
                return true;
            }
        }
        return false;
    }

    public void PickNewPlatform(ItemType type) {
        int currentPlatform = mapManager.totalPlatformsGenerated;

        if(type == ItemType.BOOST) { 
            int minPlatform = currentPlatform + spawn_Min_Boost;
            int maxPlatform = currentPlatform + spawn_Max_Boost;

            currentPlatformToSpawn_Boost = Random.Range(minPlatform, maxPlatform);
        }
        if(type == ItemType.BOMB) { 
            int minPlatform = currentPlatform + spawn_Min_Bomb;
            int maxPlatform = currentPlatform + spawn_Max_Bomb;

            currentPlatformToSpawn_Bomb = Random.Range(minPlatform, maxPlatform);
        }
    }

}
